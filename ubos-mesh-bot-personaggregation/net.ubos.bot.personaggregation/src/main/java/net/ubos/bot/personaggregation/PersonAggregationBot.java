//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.personaggregation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.ubos.bot.Bot;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectBlessChange;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.Google.GoogleSubjectArea;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.Person.PersonSubjectArea;
import net.ubos.model.Structure.StructureSubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.logging.Log;

/**
 * This Bot connects information about people received from various imports (e.g.
 * Google, Facebook) to the canonical "address book" of people.
 *
 * FIXME: this can be made much better and much more complete.
 */
public class PersonAggregationBot
    implements
        Bot
{
    private static final Log log = Log.getLogInstance( PersonAggregationBot.class );

    /**
    * {@inheritDoc}
    */
    @Override
    public void notifyChanges(
            ChangeList changes )
    {
        // try to quit as early as possible

        List<MeshObject> relevantGoogleObjects   = new ArrayList<>();
        List<MeshObject> relevantFacebookObjects = new ArrayList<>();

        for( Change change : changes ) {
            if( change instanceof MeshObjectBlessChange ) {
                MeshObjectBlessChange realChange = (MeshObjectBlessChange) change;

                for( EntityType addedType : realChange.getEntityTypes() ) {
                    if( addedType.isSubtypeOfOrEquals( GoogleSubjectArea.GOOGLEACCOUNTPROFILE )) {
                        relevantGoogleObjects.add( realChange.getSource() );

                    } else if( addedType.isSubtypeOfOrEquals( GoogleSubjectArea.GOOGLEADDRESSBOOKENTRY )) {
                        relevantGoogleObjects.add( realChange.getSource() );

                    } else if( addedType.isSubtypeOfOrEquals( FacebookSubjectArea.FACEBOOKACCOUNTPROFILE )) {
                        relevantFacebookObjects.add( realChange.getSource() );

                    }
                }
            }
        }

        if( relevantGoogleObjects.isEmpty() && relevantFacebookObjects.isEmpty() ) {
            return;
        }

        MeshBase mb = changes.getMeshBase();
        if( mb.getMeshBaseIndex() == null ) {
            // can't do without
            return;
        }

        HashMap<String,Set<MeshObject>> personsMap = new HashMap<>(); // caches people

        mb.sudoExecute( () -> {

            MeshObject people = mb.findMeshObjectByIdentifier( PERSON_PREFIX );
            if( people == null ) {
                people = mb.createMeshObject( PERSON_PREFIX, PersonSubjectArea.PERSONCOLLECTION );
                people.setPropertyValue( PersonSubjectArea.PERSONCOLLECTION_NAME, StringValue.create( "Contacts" ));
            }

            if( !relevantGoogleObjects.isEmpty() ) {
                googleObjectsAppeared( relevantGoogleObjects, people, personsMap, mb );
            }
            if( !relevantFacebookObjects.isEmpty() ) {
                facebookObjectsAppeared( relevantFacebookObjects, people, personsMap, mb );
            }
        });
    }

    /**
     * Callback: new relevant Google objects have shown up
     *
     * @param objs the MeshObjects
     * @param mb the MeshBase
     */
    protected void googleObjectsAppeared(
            List<MeshObject>            objs,
            MeshObject                  people,
            Map<String,Set<MeshObject>> personsMap,
            MeshBase                    mb )
    {
        for( MeshObject obj : objs ) {
            String fullName = null;
            if( obj.isBlessedBy( GoogleSubjectArea.GOOGLEACCOUNTPROFILE )) {
                StringValue fullNameValue = (StringValue) obj.getPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME );
                if( fullNameValue != null ) {
                    fullName = fullNameValue.value();
                }
            } else if( obj.isBlessedBy( GoogleSubjectArea.GOOGLEADDRESSBOOKENTRY )) {
                StringValue fullNameValue = (StringValue) obj.getPropertyValue( GoogleSubjectArea.GOOGLEADDRESSBOOKENTRY_FULLNAME );
                if( fullNameValue != null ) {
                    fullName = fullNameValue.value();
                }
            }
            if( fullName != null ) {
                Set<MeshObject> candidates = searchForPerson( fullName, personsMap, mb );

                MeshObject person;
                boolean    created = false;

                if( candidates == null || candidates.isEmpty() ) {
                    person = mb.createMeshObject( mb.createRandomMeshObjectIdentifierBelow( people.getIdentifier() ), PersonSubjectArea.PERSON );
                    person.setAttributeValue( CREATOR, CREATOR_NAME );
                    created = true;

                } else if( candidates.size() == 1 ) {
                    person = candidates.iterator().next();

                } else {
                    person = candidates.iterator().next();
                    log.warn( "More than one candidate, picking one at random:", fullName, person );
                }

                if( person != null ) {
                    // fill in information from elsewhere if we don't have it already,
                    // and relate

                    if( person.getPropertyValue( PersonSubjectArea.PERSON_FULLNAME ) == null ) {
                        person.setPropertyValue( PersonSubjectArea.PERSON_FULLNAME, StringValue.create( fullName ));

                        addPersonToMap( person, personsMap );
                    }

                    if( obj.isBlessedBy( GoogleSubjectArea.GOOGLEACCOUNTPROFILE )) {
                        if( person.getPropertyValue( PersonSubjectArea.PERSON_FIRSTNAME ) == null ) {
                            person.setPropertyValue(
                                    PersonSubjectArea.PERSON_FIRSTNAME,
                                    obj.getPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FIRSTNAME ) );
                        }
                        if( person.getPropertyValue( PersonSubjectArea.PERSON_LASTNAME ) == null ) {
                            person.setPropertyValue(
                                    PersonSubjectArea.PERSON_LASTNAME,
                                    obj.getPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_LASTNAME ) );
                        }
                        person.blessRole( PersonSubjectArea.PERSON_USES_ANY_S, obj );
                        person.setRoleAttributeValue( obj, CREATOR, CREATOR_NAME );

                    } else if( obj.isBlessedBy( GoogleSubjectArea.GOOGLEADDRESSBOOKENTRY )) {
                        if( person.getPropertyValue( PersonSubjectArea.PERSON_FIRSTNAME ) == null ) {
                            person.setPropertyValue(
                                    PersonSubjectArea.PERSON_FIRSTNAME,
                                    obj.getPropertyValue( GoogleSubjectArea.GOOGLEADDRESSBOOKENTRY_FIRSTNAME ) );
                        }
                        if( person.getPropertyValue( PersonSubjectArea.PERSON_LASTNAME ) == null ) {
                            person.setPropertyValue(
                                    PersonSubjectArea.PERSON_LASTNAME,
                                    obj.getPropertyValue( GoogleSubjectArea.GOOGLEADDRESSBOOKENTRY_LASTNAME ) );
                        }
                        person.blessRole( PersonSubjectArea.PERSON_USES_ANY_S, obj );
                        person.setRoleAttributeValue( obj, CREATOR, CREATOR_NAME );

                    } else {
                        log.error( "Should not be here" );
                    }

                    if( created ) {
                        people.blessRole( StructureSubjectArea.COLLECTION_COLLECTS_ANY_S, person );
                    }
                }
            }
        }
    }

    /**
     * Callback: new relevant Facebook objects have shown up
     *
     * @param objs the MeshObjects
     * @param mb the MeshBase
     */
    protected void facebookObjectsAppeared(
            List<MeshObject>            objs,
            MeshObject                  people,
            Map<String,Set<MeshObject>> personsMap,
            MeshBase                    mb )
    {
        for( MeshObject obj : objs ) {
            String      fullName = null;
            StringValue fullNameValue = (StringValue) obj.getPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME );
            if( fullNameValue != null ) {
                fullName = fullNameValue.value();
            }

            if( fullName != null ) {
                Set<MeshObject> candidates = searchForPerson( fullName, personsMap, mb );

                MeshObject person;
                boolean    created = false;

                if( candidates == null || candidates.isEmpty() ) {
                    person = mb.createMeshObject( mb.createRandomMeshObjectIdentifierBelow( people.getIdentifier() ), PersonSubjectArea.PERSON );
                    person.setAttributeValue( CREATOR, CREATOR_NAME );
                    created = true;

                } else if( candidates.size() == 1 ) {
                    person = candidates.iterator().next();

                } else {
                    person = candidates.iterator().next();
                    log.warn( "More than one candidate, picking one at random:", fullName, person );
                }

                if( person != null ) {
                    // fill in information from elsewhere if we don't have it already,
                    // and relate

                    if( person.getPropertyValue( PersonSubjectArea.PERSON_FULLNAME ) == null ) {
                        person.setPropertyValue( PersonSubjectArea.PERSON_FULLNAME, StringValue.create( fullName ));

                        addPersonToMap( person, personsMap );
                    }

                    if( person.getPropertyValue( PersonSubjectArea.PERSON_FIRSTNAME ) == null ) {
                        person.setPropertyValue(
                                PersonSubjectArea.PERSON_FIRSTNAME,
                                obj.getPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FIRSTNAME ) );
                    }
                    if( person.getPropertyValue( PersonSubjectArea.PERSON_LASTNAME ) == null ) {
                        person.setPropertyValue(
                                PersonSubjectArea.PERSON_LASTNAME,
                                obj.getPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_LASTNAME ) );
                    }
                    person.blessRole( PersonSubjectArea.PERSON_USES_ANY_S, obj );
                    person.setRoleAttributeValue( obj, CREATOR, CREATOR_NAME );

                    if( created ) {
                        people.blessRole( StructureSubjectArea.COLLECTION_COLLECTS_ANY_S, person );
                    }
                }
            }
        }
    }

    /**
     * Helper that looks for a Person. This may not scale for very large address books but works for now.
     */
    protected Set<MeshObject> searchForPerson(
            String                      fullName,
            Map<String,Set<MeshObject>> personsMap,
            MeshBase                    mb )
    {
        MeshBaseIndex index = mb.getMeshBaseIndex();
        if( personsMap.isEmpty() ) { // only happens on the first call
            MeshObjectSet persons = index.searchForBlessedWithEntityType( PersonSubjectArea.PERSON );
            for( MeshObject person : persons ) {
                addPersonToMap( person, personsMap );
            }
        }

        String [] nameComponents = fullName.split( "\\s+" );

        Set<MeshObject> currentSet = null;
        for( String nameComponent : nameComponents ) {
            String key = nameComponent.toLowerCase();
            Set<MeshObject> found = personsMap.get( key );

            if( found == null || found.isEmpty()) {
                return null; // no point continuing
            }

            if( currentSet == null ) {
                currentSet = found;

            } else {
                // no .intersect method in Set...
                HashSet<MeshObject> newCurrent = new HashSet<>();
                for( MeshObject current : currentSet ) {
                    if( found.contains( current )) {
                        newCurrent.add( current );
                    }
                }
                if( newCurrent.isEmpty() ) {
                    return null; // no point
                }
                currentSet = newCurrent;
            }
        }
        return currentSet;
    }

    /**
     * Helper to add a Person to the map.
     */
    protected void addPersonToMap(
            MeshObject                  person,
            Map<String,Set<MeshObject>> personsMap )
    {
        StringValue found = (StringValue) person.getPropertyValue( PersonSubjectArea.PERSON_FULLNAME );
        if( found != null ) {
            for( String name : found.value().split( "\\s+" )) {
                String key = name.toLowerCase();

                Set<MeshObject> already = personsMap.get( key );
                if( already == null ) {
                    already = new HashSet<>();
                    personsMap.put( key, already );
                }
                already.add(  person );
            }
        }
    }

    /**
     * Prefix for MeshObjectIdentifiers that represent people.
     */
    public static final String PERSON_PREFIX = "peers";

    /**
     * The Attribute name indicating who did this.
     */
    public static final String CREATOR = "bot-created";

    /**
     * The Attribute value for me as creator.
     */
    public static final String CREATOR_NAME = PersonAggregationBot.class.getName();
}
