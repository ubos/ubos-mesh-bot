//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.personaggregation;

import net.ubos.daemon.Daemon;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleDeactivationException;

/**
 * Module initializer class.
*/
public class ModuleInit
{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        if( theBot == null ) {
            theBot = new PersonAggregationBot();
        }
        Daemon.registerBot( theBot );
    }

    /**
     * Diet4j module deactivation.
     *
     * @param thisModule the Module being deactivated
     * @throws ModuleDeactivationException thrown if module deactivation failed
     */
    public static void moduleDectivate(
            Module thisModule )
        throws
            ModuleDeactivationException
    {
        Daemon.unregisterBot( theBot );
    }

   /**
     * My Bot.
     */
    protected static PersonAggregationBot theBot;
}
