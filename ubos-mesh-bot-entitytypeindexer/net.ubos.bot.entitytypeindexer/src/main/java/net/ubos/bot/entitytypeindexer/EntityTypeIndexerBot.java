//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.entitytypeindexer;

import java.util.List;
import net.ubos.bot.Bot;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseWellKnown;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectBlessChange;
import net.ubos.meshbase.transaction.MeshObjectCreateChange;
import net.ubos.meshbase.transaction.MeshObjectDeleteChange;
import net.ubos.meshbase.transaction.MeshObjectUnblessChange;
import net.ubos.model.primitives.EntityType;

/**
 * This Bot indexes MeshObjects blessed with EntityTypes.
 */
public class EntityTypeIndexerBot
    implements
        Bot
{
    /**
    * {@inheritDoc}
    */
    @Override
    public void notifyChanges(
            ChangeList changes )
    {
        if( changes.isEmpty() ) {
            return;
        }

        MeshBase      mb    = changes.getMeshBase();
        MeshBaseIndex index = mb.getMeshBaseIndex();

        mb.sudoExecute( () -> {
            List<Change> changeList = changes.getChanges();
            for( int i=0 ; i<changeList.size() ; ++i ) { // use index, better for debugging
                Change change = changeList.get( i );

                if( change instanceof MeshObjectCreateChange ) {
                    MeshObjectCreateChange realChange = (MeshObjectCreateChange) change;
                    MeshObject             obj        = realChange.getChangedMeshObject();

                    addToTypeIndex( null, obj, index );

                } else if( change instanceof MeshObjectDeleteChange ) {
                    MeshObjectDeleteChange realChange = (MeshObjectDeleteChange) change;
                    MeshObject             obj        = realChange.getChangedMeshObject();

                    removeFromTypeIndex( null, obj, index );


                } else if( change instanceof MeshObjectBlessChange ) {
                    MeshObjectBlessChange realChange = (MeshObjectBlessChange) change;
                    MeshObject            obj        = realChange.getSource();

                    for( EntityType et : realChange.getDeltaValue() ) {
                        addToTypeIndex( et, obj, index );
                    }
                    if( realChange.getOldValue().length == 0 ) {
                        removeFromTypeIndex( null, obj, index );
                    }

                } else if( change instanceof MeshObjectUnblessChange ) {
                    MeshObjectUnblessChange realChange = (MeshObjectUnblessChange) change;
                    MeshObject              obj        = realChange.getSource();

                    for( EntityType et : realChange.getDeltaValue() ) {
                        removeFromTypeIndex( et, obj, index );
                    }
                    if( realChange.getNewValue().length == 0 ) {
                        addToTypeIndex( null, obj, index );
                    }
                }
            }
        });
    }

    /**
     * Helper method to add to the EntityType index.
     * 
     * @param entry the EntityType, or null if untyped
     * @param obj the MeshObject
     * @param index the MeshBaseIndex to use
     */
    protected static void addToTypeIndex(
            EntityType    entry,
            MeshObject    obj,
            MeshBaseIndex index )
    {
        MeshObject typeObj = index.obtainTypeObject( entry );
        if( typeObj != obj ) { // happens for the unblessed type object
            typeObj.setRoleAttributeValue( obj, MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE, (long) 1 );
        }
    }

    /**
     * Helper method to remove from the EntityType index.

     * @param entry the EntityType, or null if untyped
     * @param obj the MeshObject
     * @param index the MeshBaseIndex to use
     */
    protected static void removeFromTypeIndex(
            EntityType    entry,
            MeshObject    obj,
            MeshBaseIndex index )
    {
        MeshObject typeObj = index.obtainTypeObject( entry );
        typeObj.deleteRoleAttribute( obj, MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE );
    }
}
