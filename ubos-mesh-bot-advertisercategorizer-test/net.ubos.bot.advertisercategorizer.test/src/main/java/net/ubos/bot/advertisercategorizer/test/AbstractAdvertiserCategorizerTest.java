//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.advertisercategorizer.test;

import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality for AdvertiserCategorizerTests.
 */
public abstract class AbstractAdvertiserCategorizerTest
        extends
            AbstractTest
{
    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        thePrimaryNsMap = MPrimaryMeshObjectIdentifierNamespaceMap.create();

        theMeshBase = MMeshBase.Builder.create().namespaceMap( thePrimaryNsMap ).build();
    }

    /**
     * Clean up after the test.
     */
    @AfterEach
    public void cleanup()
    {
        theMeshBase.die();
    }

    /**
     * The primary namespace map.
     */
    protected MPrimaryMeshObjectIdentifierNamespaceMap thePrimaryNsMap;

    /**
     * The MeshBase for the test.
     */
    protected MeshBase theMeshBase;
}
