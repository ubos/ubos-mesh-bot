//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.advertisercategorizer.test;

import java.util.ArrayList;
import java.util.HashMap;
import net.ubos.bot.advertisercategorizer.AdvertiserCategorizerBot;
import net.ubos.mesh.MeshObject;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the splitting of names and categorization.
 */
public class AdvertiserCategorizerTest1
    extends
        AbstractAdvertiserCategorizerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating advertisers" );

        String [] patterns = {
            "a b c d", // 0
            "a b c",   // 1
            "b c",     // 2
            "c b",     // 3
            "a c",     // 4
            "a c d",   // 5
            "a d",     // 6
            "n1 a n2 - c & d n3" // 7 was: "Fremont Motor Riverton - Ford & Lincoln Dealer"
        };

        ArrayList<MeshObject> advs = new ArrayList<>();

        theMeshBase.execute( () -> {
            for( int i=0 ; i<patterns.length ; ++i ) {
                MeshObject adv = theMeshBase.createMeshObject( patterns[i].replaceAll( " ", "" ), MarketingSubjectArea.ADVERTISER );
                adv.setPropertyValue( MarketingSubjectArea.ADVERTISER_NAME, StringValue.create( patterns[i] ));

                advs.add(  adv );
            }
        } );

        //

        log.info( "Running algorithm" );

        theMeshBase.execute( () -> {
            for( int i=0 ; i<patterns.length ; ++i ) {

                MeshObject adv = advs.get( i );

                log.info( "Running algorithm: " + i );

                AdvertiserCategorizerBot.placeInCategories( adv, theCategories, AdvertiserCategorizerBot::findOrCreateCategory );
            }
        } );

        //

        log.info( "Checking" );

        MeshObject abcd_cat = theMeshBase.findMeshObjectByIdentifierOrThrow( "industries/category/abcd" );

        MeshObject abcd = advs.get( 0 );
        checkEquals( abcd.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).size(), 1, "Not in 1 category" );
        checkCondition( abcd.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( abcd_cat ), "Wrong category" );

        MeshObject abc_cat  = theMeshBase.findMeshObjectByIdentifierOrThrow( "industries/category/abc" );

        MeshObject abc = advs.get( 1 );
        checkEquals( abc.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).size(), 1, "Not in 1 category" );
        checkCondition( abc.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( abc_cat ), "Wrong category" );

        MeshObject bc_cat   = theMeshBase.findMeshObjectByIdentifierOrThrow( "industries/category/bc" );

        MeshObject bc = advs.get( 2 );
        checkEquals( bc.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).size(), 1, "Not in 1 category" );
        checkCondition( bc.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( bc_cat ), "Wrong category" );

        MeshObject c_cat    = theMeshBase.findMeshObjectByIdentifierOrThrow( "industries/category/c" );

        MeshObject cb = advs.get( 3 );
        checkEquals( cb.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).size(), 1, "Not in 1 category" );
        checkCondition( cb.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( c_cat ), "Wrong category" );

        MeshObject a_cat    = theMeshBase.findMeshObjectByIdentifierOrThrow( "industries/category/a" );

        MeshObject ac = advs.get( 4 );
        checkEquals( ac.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).size(), 2, "Not in 2 categories" );
        checkCondition( ac.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( a_cat ), "Wrong category" );
        checkCondition( ac.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( c_cat ), "Wrong category" );

        MeshObject cd_cat   = theMeshBase.findMeshObjectByIdentifierOrThrow( "industries/category/cd" );

        MeshObject acd = advs.get( 5 );
        checkEquals( acd.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).size(), 2, "Not in 2 categories" );
        checkCondition( acd.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( a_cat ),  "Wrong category" );
        checkCondition( acd.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( cd_cat ), "Wrong category" );

        MeshObject d_cat    = theMeshBase.findMeshObjectByIdentifierOrThrow( "industries/category/d" );

        MeshObject ad = advs.get( 6 );
        checkEquals( ad.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).size(), 2, "Not in 2 categories" );
        checkCondition( ad.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( a_cat ), "Wrong category" );
        checkCondition( ad.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( d_cat ), "Wrong category" );

        MeshObject other1 = advs.get( 7 );
        checkEquals( other1.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).size(), 3, "Not in 3 categories" );
        checkCondition( other1.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( a_cat ), "Wrong category" );
        checkCondition( other1.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( c_cat ), "Wrong category" );
        checkCondition( other1.traverse( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S ).contains( d_cat ), "Wrong category" );
    }

    /**
     * The category table to use.
     */
    protected static final HashMap<String,String> theCategories = new HashMap<>();
    static {
        theCategories.put( "a b c d", "abcd" );
        theCategories.put( "a b c",   "abc" );
        theCategories.put( "b c d",   "bcd" );
        theCategories.put( "d b a",   "dba" );
        theCategories.put( "b c",     "bc" );
        theCategories.put( "c d",     "cd" );
        theCategories.put( "a",       "a" );
        theCategories.put( "c",       "c" );
        theCategories.put( "d",       "d" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( AdvertiserCategorizerTest1.class );
}
