//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.entitytypeindexer.test;

import java.util.Iterator;
import java.util.Set;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the EntityTypeIndexer with increments.
 */
public class EntityTypeIndexerTest2
    extends
        AbstractEntityTypeIndexerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating the index" );

        MeshBaseIndex index = MeshBaseIndex.create( theMeshBase );

        MeshObjectSet aaTypes = index.searchForBlessedWithEntityType( TestSubjectArea.AA );
        MeshObjectSet bTypes  = index.searchForBlessedWithEntityType( TestSubjectArea.B );

        checkEquals( aaTypes.size(), 0, "Have AAs" );
        checkEquals( bTypes.size(),  0, "Have Bs" );

        //

        log.info( "Creating MeshObjects (1) and checking" );

        theMeshBase.execute( () -> {
            MeshObject aaaa1 = theMeshBase.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject aaaa2 = theMeshBase.createMeshObject( "aaaa2", TestSubjectArea.AA );

            MeshObject bbbb1 = theMeshBase.createMeshObject( "bbbb1", TestSubjectArea.B );

            theMeshBase.createMeshObject( "oooo1" );

            theMeshBase.createMeshObject( "cccc1" ).bless( new EntityType[] { TestSubjectArea.B } );
        } );

        aaTypes      = index.searchForBlessedWithEntityType( TestSubjectArea.AA );
        bTypes       = index.searchForBlessedWithEntityType( TestSubjectArea.B );

        checkEqualsOutOfSequence(
                asIdStrings( aaTypes.asSet() ),
                new String[] { "aaaa1", "aaaa2" },
                "Wrong AAs" );
        checkEqualsOutOfSequence(
                asIdStrings( bTypes.asSet() ),
                new String[] { "bbbb1", "cccc1" },
                "Wrong Bs" );

        //

        log.info( "Creating/updating MeshObjects (2) and checking" );

        theMeshBase.execute( () -> {
            MeshObject aaaa2 = theMeshBase.findMeshObjectByIdentifier( "aaaa2" );
            MeshObject aaaa3 = theMeshBase.createMeshObject( "aaaa3", TestSubjectArea.AA );

            MeshObject bbbb2 = theMeshBase.createMeshObject( "bbbb2", TestSubjectArea.B );
            MeshObject bbbb3 = theMeshBase.createMeshObject( "bbbb3", TestSubjectArea.B );
            MeshObject bbbb4 = theMeshBase.createMeshObject( "bbbb4", TestSubjectArea.B );
            MeshObject bbbb5 = theMeshBase.createMeshObject( "bbbb5", TestSubjectArea.B );

            theMeshBase.createMeshObject( "oooo2" );

            MeshObject cccc1 = theMeshBase.findMeshObjectByIdentifier( "cccc1" );
            cccc1.bless( TestSubjectArea.AA );
            theMeshBase.createMeshObject( "cccc2" ).bless( new EntityType[] { TestSubjectArea.AA, TestSubjectArea.B } );
        } );

        aaTypes      = index.searchForBlessedWithEntityType( TestSubjectArea.AA );
        bTypes       = index.searchForBlessedWithEntityType( TestSubjectArea.B );

        checkEqualsOutOfSequence(
                asIdStrings( aaTypes.asSet() ),
                new String[] { "aaaa1", "aaaa2", "aaaa3", "cccc1", "cccc2" },
                "Wrong AAs" );
        checkEqualsOutOfSequence(
                asIdStrings( bTypes.asSet() ),
                new String[] { "bbbb1", "bbbb2", "bbbb3", "bbbb4", "bbbb5", "cccc1", "cccc2" },
                "Wrong Bs" );
    }

    /**
     * Helper to convert.
     *
     * @param in
     * @return transformed
     */
    protected String [] asIdStrings(
            Set<MeshObject> in )
    {
        String []            ret  = new String[ in.size() ];
        Iterator<MeshObject> iter = in.iterator();

        for( int i=0 ; iter.hasNext() ; ++i ) {
            ret[i] = ExternalNameHashMeshObjectIdentifierSerializer.SINGLETON.toExternalForm( iter.next().getIdentifier());
        }
        return ret;
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( EntityTypeIndexerTest2.class );
}
