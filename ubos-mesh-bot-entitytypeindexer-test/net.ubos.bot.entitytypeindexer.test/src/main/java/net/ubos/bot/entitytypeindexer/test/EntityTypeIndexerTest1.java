//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.entitytypeindexer.test;

import java.util.Iterator;
import java.util.Set;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the EntityTypeIndexer in a single run.
 */
public class EntityTypeIndexerTest1
    extends
        AbstractEntityTypeIndexerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshObjects" );

        theMeshBase.execute( () -> {
            MeshObject aaaa1 = theMeshBase.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject aaaa2 = theMeshBase.createMeshObject( "aaaa2", TestSubjectArea.AA );
            MeshObject aaaa3 = theMeshBase.createMeshObject( "aaaa3", TestSubjectArea.AA );

            MeshObject bbbb1 = theMeshBase.createMeshObject( "bbbb1", TestSubjectArea.B );
            MeshObject bbbb2 = theMeshBase.createMeshObject( "bbbb2", TestSubjectArea.B );
            MeshObject bbbb3 = theMeshBase.createMeshObject( "bbbb3", TestSubjectArea.B );
            MeshObject bbbb4 = theMeshBase.createMeshObject( "bbbb4", TestSubjectArea.B );
            MeshObject bbbb5 = theMeshBase.createMeshObject( "bbbb5", TestSubjectArea.B );

            theMeshBase.createMeshObject( "oooo1" );
            theMeshBase.createMeshObject( "oooo2" );

            theMeshBase.createMeshObject( "cccc1" ).bless( new EntityType[] { TestSubjectArea.AA, TestSubjectArea.B } );
            theMeshBase.createMeshObject( "cccc2" ).bless( new EntityType[] { TestSubjectArea.AA, TestSubjectArea.B } );
        } );

        //

        log.info( "Creating the index" );

        MeshBaseIndex index = MeshBaseIndex.create( theMeshBase );

        //

        log.info( "Checking the EntityType index" );

        MeshObjectSet aaTypes = index.searchForBlessedWithEntityType( TestSubjectArea.AA );
        checkEqualsOutOfSequence(
                asIdStrings( aaTypes.asSet() ),
                new String[] { "aaaa1", "aaaa2", "aaaa3", "cccc1", "cccc2" },
                "Wrong AAs" );

        MeshObjectSet bTypes = index.searchForBlessedWithEntityType( TestSubjectArea.B );
        checkEqualsOutOfSequence(
                asIdStrings( bTypes.asSet() ),
                new String[] { "bbbb1", "bbbb2", "bbbb3", "bbbb4", "bbbb5", "cccc1", "cccc2" },
                "Wrong Bs" );
    }

    /**
     * Helper to convert.
     *
     * @param in
     * @return transformed
     */
    protected String [] asIdStrings(
            Set<MeshObject> in )
    {
        String []            ret  = new String[ in.size() ];
        Iterator<MeshObject> iter = in.iterator();

        for( int i=0 ; iter.hasNext() ; ++i ) {
            ret[i] = ExternalNameHashMeshObjectIdentifierSerializer.SINGLETON.toExternalForm( iter.next().getIdentifier());
        }
        return ret;
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( EntityTypeIndexerTest1.class );
}
