//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.wordindexer.test;

import java.util.Iterator;
import java.util.Set;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.OrderedMeshObjectSetWithValue;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the WordIndexer with increments.
 */
public class WordIndexerTest2
    extends
        AbstractWordIndexerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating the index" );

        MeshBaseIndex index = MeshBaseIndex.create( theMeshBase );

        OrderedMeshObjectSetWithValue<Long> withBbbb     = index.searchForWordWithCount( "bbbb" );
        OrderedMeshObjectSetWithValue<Long> doesNotExist = index.searchForWordWithCount( "doesnotexist" );
        OrderedMeshObjectSetWithValue<Long> withCccc     = index.searchForWordWithCount( "cccc" );

        checkNotObject( withBbbb,     "Have withBbbb" );
        checkNotObject( doesNotExist, "Have doesNotExist" );
        checkNotObject( withCccc,     "Have cccc" );

        //

        log.info( "Creating MeshObjects (1) and checking" );

        theMeshBase.execute( () -> {
            MeshObject aaaa1 = theMeshBase.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject aaaa2 = theMeshBase.createMeshObject( "aaaa2", TestSubjectArea.AA );

            aaaa1.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa bbbb is cccc" ));
            aaaa2.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "Nobbbb is cccc" ));
            aaaa2.setPropertyValue( TestSubjectArea.A_XX, TestSubjectArea.A_XX_type.createBlobValue( "aaaabbbb".getBytes(), SelectableMimeType.OCTET_STREAM.getMimeType() ));

            MeshObject bbbb1 = theMeshBase.createMeshObject( "bbbb1", TestSubjectArea.B );

            theMeshBase.createMeshObject( "oooo1" );

            theMeshBase.createMeshObject( "cccc1" ).bless( new EntityType[] { TestSubjectArea.B } );
        } );

        withBbbb     = index.searchForWordWithCount( "bbbb" );
        doesNotExist = index.searchForWordWithCount( "doesnotexist" );
        withCccc     = index.searchForWordWithCount( "cccc" );

        checkEqualsOutOfSequence(
                asIdStrings( withBbbb.asSet() ),
                new String[] { "aaaa1" },
                "Wrong withBbbb" );
        checkEquals( withBbbb.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa1" )), 1L, "bbbb count wrong in aaaa1" );
        checkNotObject( doesNotExist, "Have doesNotExist" );
        checkEqualsOutOfSequence(
                asIdStrings( withCccc.asSet() ),
                new String[] { "aaaa1", "aaaa2" },
                "Wrong cccc" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa1" )), 1L, "bbbb count wrong in aaaa1" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa2" )), 1L, "bbbb count wrong in aaaa2" );

        //

        log.info( "Creating/updating MeshObjects (2) and checking" );

        theMeshBase.execute( () -> {
            MeshObject aaaa2 = theMeshBase.findMeshObjectByIdentifier( "aaaa2" );
            MeshObject aaaa3 = theMeshBase.createMeshObject( "aaaa3", TestSubjectArea.AA );

            aaaa2.setPropertyValue( TestSubjectArea.A_XX, TestSubjectArea.A_XX_type.createBlobValue( "Let's put aaaabbbb cccc", SelectableMimeType.TEXT_PLAIN.getMimeType() ));
            aaaa3.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "Find me cccc more than once and find me cccc" ));

            MeshObject bbbb2 = theMeshBase.createMeshObject( "bbbb2", TestSubjectArea.B );
            MeshObject bbbb3 = theMeshBase.createMeshObject( "bbbb3", TestSubjectArea.B );
            MeshObject bbbb4 = theMeshBase.createMeshObject( "bbbb4", TestSubjectArea.B );
            MeshObject bbbb5 = theMeshBase.createMeshObject( "bbbb5", TestSubjectArea.B );

            theMeshBase.createMeshObject( "oooo2" );

            MeshObject cccc1 = theMeshBase.findMeshObjectByIdentifier( "cccc1" );
            cccc1.bless( TestSubjectArea.AA );
            theMeshBase.createMeshObject( "cccc2" ).bless( new EntityType[] { TestSubjectArea.AA, TestSubjectArea.B } );
        } );

        withBbbb     = index.searchForWordWithCount( "bbbb" );
        doesNotExist = index.searchForWordWithCount( "doesnotexist" );
        withCccc     = index.searchForWordWithCount( "cccc" );

        checkEqualsOutOfSequence(
                asIdStrings( withBbbb.asSet() ),
                new String[] { "aaaa1" },
                "Wrong withBbbb" );
        checkEquals( withBbbb.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa1" )), 1L, "bbbb count wrong in aaaa1" );
        checkNotObject( doesNotExist, "Have doesNotExist" );
        checkEqualsOutOfSequence(
                asIdStrings( withCccc.asSet() ),
                new String[] { "aaaa1", "aaaa2", "aaaa3" },
                "Wrong cccc" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa1" )), 1L, "bbbb count wrong in aaaa1" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa2" )), 1L, "bbbb count wrong in aaaa2" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa3" )), 2L, "bbbb count wrong in aaaa3" );
    }

    /**
     * Helper to convert.
     *
     * @param in
     * @return transformed
     */
    protected String [] asIdStrings(
            Set<MeshObject> in )
    {
        String []            ret  = new String[ in.size() ];
        Iterator<MeshObject> iter = in.iterator();

        for( int i=0 ; iter.hasNext() ; ++i ) {
            ret[i] = ExternalNameHashMeshObjectIdentifierSerializer.SINGLETON.toExternalForm( iter.next().getIdentifier());
        }
        return ret;

    }

    // Our Logger
    private static final Log log = Log.getLogInstance( WordIndexerTest2.class );
}
