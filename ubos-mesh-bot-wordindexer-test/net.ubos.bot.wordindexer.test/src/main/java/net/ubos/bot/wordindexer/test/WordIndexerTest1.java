//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.wordindexer.test;

import java.util.Iterator;
import java.util.Set;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.OrderedMeshObjectSetWithValue;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the WordIndexer in a single run.
 */
public class WordIndexerTest1
    extends
        AbstractWordIndexerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshObjects" );

        theMeshBase.execute( () -> {
            MeshObject aaaa1 = theMeshBase.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject aaaa2 = theMeshBase.createMeshObject( "aaaa2", TestSubjectArea.AA );
            MeshObject aaaa3 = theMeshBase.createMeshObject( "aaaa3", TestSubjectArea.AA );

            aaaa1.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa bbbb is cccc" ));
            aaaa2.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "xxbbbb is cccc" ));
            aaaa2.setPropertyValue( TestSubjectArea.A_XX, TestSubjectArea.A_XX_type.createBlobValue( "Let's put aaaabbbb cccc", SelectableMimeType.TEXT_PLAIN.getMimeType() ));
            aaaa3.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "Find me here cccc than once and find me cccc" ));
            aaaa3.setPropertyValue( TestSubjectArea.A_XX, TestSubjectArea.A_XX_type.createBlobValue( "Aaaabbbb".getBytes(), SelectableMimeType.OCTET_STREAM.getMimeType() ));

            MeshObject bbbb1 = theMeshBase.createMeshObject( "bbbb1", TestSubjectArea.B );
            MeshObject bbbb2 = theMeshBase.createMeshObject( "bbbb2", TestSubjectArea.B );
            MeshObject bbbb3 = theMeshBase.createMeshObject( "bbbb3", TestSubjectArea.B );
            MeshObject bbbb4 = theMeshBase.createMeshObject( "bbbb4", TestSubjectArea.B );
            MeshObject bbbb5 = theMeshBase.createMeshObject( "bbbb5", TestSubjectArea.B );

            bbbb3.setPropertyValue( TestSubjectArea.B_U, StringValue.create( "some other bbbb cccc") );

            theMeshBase.createMeshObject( "oooo1" );
            theMeshBase.createMeshObject( "oooo2" );

            theMeshBase.createMeshObject( "cccc1" ).bless( new EntityType[] { TestSubjectArea.AA, TestSubjectArea.B } );
            theMeshBase.createMeshObject( "cccc2" ).bless( new EntityType[] { TestSubjectArea.AA, TestSubjectArea.B } );
        } );

        //

        log.info( "Creating the index" );

        MeshBaseIndex index = MeshBaseIndex.create( theMeshBase );

        //

        log.info( "Checking the word index" );

        OrderedMeshObjectSetWithValue<Long> withIs = index.searchForWordWithCount( "is" ); // stop word
        checkNotObject( withIs, "Found occurrence of stop word" );

        OrderedMeshObjectSetWithValue<Long> withBbbb = index.searchForWordWithCount( "bbbb" );
        checkEqualsOutOfSequence(
                asIdStrings( withBbbb.asSet() ),
                new String[] { "aaaa1", "bbbb3" },
                "Wrong result for bbbb" );
        checkEquals( withBbbb.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa1" )), 1L, "bbbb count wrong in aaaa1" );
        checkEquals( withBbbb.getValueFor( theMeshBase.findMeshObjectByIdentifier( "bbbb3" )), 1L, "bbbb count wrong in bbbb3"  );

        OrderedMeshObjectSetWithValue<Long> doesNotExist = index.searchForWordWithCount( "doesnotexist" );
        checkNotObject( doesNotExist, "Found something that does not exist" );

        OrderedMeshObjectSetWithValue<Long> withCccc = index.searchForWordWithCount( "cccc" );
        checkEqualsOutOfSequence(
                asIdStrings( withCccc.asSet()),
                new String[] { "aaaa1", "aaaa2", "aaaa3", "bbbb3" },
                "Wrong result for cccc" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa1" )), 1L, "cccc count wrong in aaaa1" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa2" )), 1L, "cccc count wrong in aaaa2" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "aaaa3" )), 2L, "cccc count wrong in aaaa3" );
        checkEquals( withCccc.getValueFor( theMeshBase.findMeshObjectByIdentifier( "bbbb3" )), 1L, "cccc count wrong in bbbb3" );
    }

    /**
     * Helper to convert.
     *
     * @param in
     * @return transformed
     */
    protected String [] asIdStrings(
            Set<MeshObject> in )
    {
        String []            ret  = new String[ in.size() ];
        Iterator<MeshObject> iter = in.iterator();

        for( int i=0 ; iter.hasNext() ; ++i ) {
            ret[i] = ExternalNameHashMeshObjectIdentifierSerializer.SINGLETON.toExternalForm( iter.next().getIdentifier());
        }
        return ret;
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( WordIndexerTest1.class );
}
