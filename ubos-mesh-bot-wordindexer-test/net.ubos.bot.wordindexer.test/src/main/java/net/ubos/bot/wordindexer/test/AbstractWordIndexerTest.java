//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.wordindexer.test;

import net.ubos.bot.wordindexer.WordIndexerBot;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.TransactionListener;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality for WordIndexerTests.
 */
public abstract class AbstractWordIndexerTest
        extends
            AbstractTest
{
    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        theMeshBase = MMeshBase.Builder.create().build();

        theBot = new WordIndexerBot();
        theMeshBase.addDirectTransactionListener( new TransactionListener() {
            @Override
            public void transactionCommitted(
                    HeadTransaction tx )
            {
                theBot.notifyChanges( tx.getChangeList() );
            }
        } );
    }

    /**
     * Clean up after the test.
     */
    @AfterEach
    public void cleanup()
    {
        theMeshBase.die();
    }

    /**
     * The MeshBase for the test.
     */
    protected MeshBase theMeshBase;
    
    /**
     * The bot to be tested.
     */
    protected WordIndexerBot theBot;
}
