//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.wordindexer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import net.ubos.bot.Bot;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseWellKnown;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.meshbase.transaction.MeshObjectPropertyChange;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;

/**
 * This Bot indexes MeshObjects that contain words.
 */
public class WordIndexerBot
    implements
        Bot
{
    /**
    * {@inheritDoc}
    */
    @Override
    public void notifyChanges(
            ChangeList changes )
    {
        if( changes.isEmpty() ) {
            return;
        }

        MeshBase      mb    = changes.getMeshBase();
        MeshBaseIndex index = mb.getMeshBaseIndex();

        mb.sudoExecute( () -> {
            List<Change> changeList = changes.getChanges();
            for( int i=0 ; i<changeList.size() ; ++i ) { // use index, better for debugging
                Change change = changeList.get( i );

                if( change instanceof MeshObjectPropertyChange ) {
                    // See also section on MeshObjectAttributeChange
                    MeshObjectPropertyChange realChange = (MeshObjectPropertyChange) change;
                    MeshObject               obj        = realChange.getSource();
                    PropertyValue            oldValue   = realChange.getOldValue();
                    PropertyValue            newValue   = realChange.getNewValue();

                    Map<String,Integer> oldWords = null;
                    Map<String,Integer> newWords = null;
                    if( oldValue instanceof StringValue ) {
                        oldWords = findWords( ((StringValue)oldValue).value() );

                    } else if( oldValue instanceof BlobValue && ((BlobValue)oldValue).getHasTextMimeType() ) {
                        oldWords = findWords( ((BlobValue)oldValue).getAsString() );
                    }
                    if( newValue instanceof StringValue ) {
                        newWords = findWords( ((StringValue)newValue).value() );

                    } else if( newValue instanceof BlobValue && ((BlobValue)newValue).getHasTextMimeType() ) {
                        newWords = findWords( ((BlobValue)newValue).getAsString() );
                    }

                    if( oldWords != null ) {
                        for( Map.Entry<String,Integer> word : oldWords.entrySet() ) {
                            removeFromWordIndex( word.getKey(), word.getValue(), obj, index );
                        }
                    }
                    if( newWords != null ) {
                        for( Map.Entry<String,Integer> word : newWords.entrySet() ) {
                            addToWordIndex( word.getKey(), word.getValue(), obj, index );
                        }
                    }

                } else if( change instanceof MeshObjectAttributeChange ) {
                    // we don't index Attributes that have null values, so we don't have to listen to added/removed events
                    // See also section on MeshObjectPropertyChange
                    MeshObjectAttributeChange realChange = (MeshObjectAttributeChange) change;
                    MeshObject                obj        = realChange.getSource();
                    Serializable              oldValue   = realChange.getOldValue();
                    Serializable              newValue   = realChange.getNewValue();

                    Map<String,Integer> oldWords = null;
                    Map<String,Integer> newWords = null;
                    if( oldValue instanceof String ) {
                        oldWords = findWords( (String) oldValue );
                    }
                    if( newValue instanceof String ) {
                        newWords = findWords( (String) newValue );
                    }

                    if( oldWords != null ) {
                        for( Map.Entry<String,Integer> word : oldWords.entrySet() ) {
                            removeFromWordIndex( word.getKey(), word.getValue(), obj, index );
                        }
                    }
                    if( newWords != null ) {
                        for( Map.Entry<String,Integer> word : newWords.entrySet() ) {
                            addToWordIndex( word.getKey(), word.getValue(), obj, index );
                        }
                    }
                }
            }
        });
    }

    /**
     * Find the words in a String that shall be indexed.
     *
     * @param s the String
     * @return the words, with frequency
     */
    protected Map<String,Integer> findWords(
            String s )
    {
        // strategy:
        // 1. split by white space
        // 2, eliminate leading and trailing punctuation
        // 3. if what remains isn't alnum, it doesn't get indexed

        HashMap<String,Integer> ret = new HashMap<>();

        String cleanedUp = s.trim().toLowerCase();
        for( String candidate : cleanedUp.split( "\\s+" )) {
            Matcher m = MeshBaseIndex.WORD_TO_INDEX_PATTERN.matcher( candidate );
            if( !m.matches() ) {
                continue;
            }
            String word = m.group(1);
            word = word.replaceAll( "-", "" );

            if( MeshBaseIndex.STOP_WORDS.contains( word )) {
                continue;
            }

            Integer already = ret.put( word, 1 );
            if( already != null ) {
                ret.put( word, already+1 );
            }
        }
        return ret;
    }

    /**
     * Helper method to add to the word index.
     * 
     * @param word the word
     * @param occurrences the number of occurrences in the MeshObject.
     * @param obj the MeshObject
     * @param index the MeshBaseIndex to use
     */
    protected static void addToWordIndex(
            String        word,
            long          occurrences,
            MeshObject    obj,
            MeshBaseIndex index )
    {
        MeshObject wordObj = index.obtainWordObject( word );
        wordObj.setRoleAttributeValue( obj, MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE, occurrences );
    }

    /**
     * Helper method to remove from the word index.
      *
     * @param word the word
     * @param occurrences the number of occurrences in the MeshObject.
     * @param obj the MeshObject
     * @param index the MeshBaseIndex to use
     */
    protected static void removeFromWordIndex(
            String        word,
            int           occurrences,
            MeshObject    obj,
            MeshBaseIndex index )
    {
        MeshObject wordObj = index.obtainWordObject( word );
        wordObj.deleteRoleAttribute( obj, MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE );
    }
}
