//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.urlextractor;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.bot.Bot;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectBlessChange;
import net.ubos.meshbase.transaction.MeshObjectPropertyChange;
import net.ubos.meshbase.transaction.MeshObjectUnblessChange;
import net.ubos.model.Web.WebSubjectArea;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.Log;

/**
 * This Bot extracts URLs from BlobValues, and creates appropriate
 * relationships to WebResource.
 */
public class UrlExtractorBot
    implements
        Bot
{
    private static final Log log = Log.getLogInstance( UrlExtractorBot.class );

    /**
    * {@inheritDoc}
    */
    @Override
    public void notifyChanges(
            ChangeList changes )
    {
        // URLs may also be contained in default PropertyValues, so we need to look for
        // Blessed, Unblessed, and PropertyChange events

        Map<MeshObject,Set<String>> relateThose   = new HashMap<>(); // where String is the URL
        Map<MeshObject,Set<String>> unrelateThose = new HashMap<>(); // where String is the URL

        for( Change change : changes ) {
            if( change instanceof MeshObjectBlessChange ) {
                MeshObjectBlessChange realChange = (MeshObjectBlessChange) change;
                MeshObject            obj        = realChange.getAffectedMeshObjects()[0];

                for( EntityType addedType : realChange.getDeltaValue() ) {
                    for( PropertyType pt : addedType.getPropertyTypes() ) {
                        PropertyValue defaultValue = pt.getDefaultValue();

                        if( defaultValue != null && defaultValue instanceof BlobValue && ((BlobValue)defaultValue).getHasTextMimeType() ) {
                            Set<String> extractedUrls = extractUrlsFrom( ((BlobValue)defaultValue).getAsString() );

                            if( extractedUrls != null && !extractedUrls.isEmpty() ) {
                                Set<String> already = relateThose.get( obj );
                                if( already == null ) {
                                    relateThose.put( obj, extractedUrls );
                                } else {
                                    already.addAll( extractedUrls );
                                }
                            }
                        }
                    }
                }

            } else if( change instanceof MeshObjectUnblessChange ) {
                MeshObjectUnblessChange realChange = (MeshObjectUnblessChange) change;
                MeshObject              obj        = realChange.getAffectedMeshObjects()[0];

                for( EntityType removedType : realChange.getDeltaValue() ) {
                    for( PropertyType pt : removedType.getPropertyTypes() ) {
                        PropertyValue defaultValue = pt.getDefaultValue();

                        if( defaultValue != null && defaultValue instanceof BlobValue && ((BlobValue)defaultValue).getHasTextMimeType() ) {
                            Set<String> extractedUrls = extractUrlsFrom( ((BlobValue)defaultValue).getAsString() );

                            if( extractedUrls != null && !extractedUrls.isEmpty() ) {
                                Set<String> already = unrelateThose.get( obj );
                                if( already == null ) {
                                    unrelateThose.put( obj, extractedUrls );
                                } else {
                                    already.addAll( extractedUrls );
                                }
                            }
                        }
                    }
                }

            } else if( change instanceof MeshObjectPropertyChange ) {
                MeshObjectPropertyChange realChange = (MeshObjectPropertyChange) change;
                MeshObject               obj        = realChange.getAffectedMeshObjects()[0];

                PropertyValue oldValue = realChange.getOldValue();
                PropertyValue newValue = realChange.getNewValue();

                if( oldValue != null && oldValue instanceof BlobValue && ((BlobValue)oldValue).getHasTextMimeType() ) {
                    Set<String> extractedOldUrls = extractUrlsFrom( ((BlobValue)oldValue).getAsString() );

                    if( extractedOldUrls != null && !extractedOldUrls.isEmpty() ) {
                        Set<String> already = unrelateThose.get( obj );
                        if( already == null ) {
                            unrelateThose.put(  obj, extractedOldUrls );
                        } else {
                            already.addAll( extractedOldUrls );
                        }
                    }
                }

                if( newValue != null && newValue instanceof BlobValue && ((BlobValue)newValue).getHasTextMimeType() ) {
                    Set<String> extractedNewUrls = extractUrlsFrom( ((BlobValue)newValue).getAsString() );

                    if( extractedNewUrls != null && !extractedNewUrls.isEmpty() ) {
                        Set<String> already = relateThose.get( obj );
                        if( already == null ) {
                            relateThose.put(  obj, extractedNewUrls );
                        } else {
                            already.addAll( extractedNewUrls );
                        }
                    }
                }
            } // currently does not deal with RoleProperties.
        }

        if( relateThose.isEmpty()  && unrelateThose.isEmpty()) {
            return;
        }


        MeshBase mb = changes.getMeshBase();
        mb.sudoExecute( () -> {

            for( Map.Entry<MeshObject,Set<String>> relate : relateThose.entrySet() ) {
                MeshObject obj = relate.getKey();
                if( obj.getIsDead() ) { // not sure this can happen here?
                    continue;
                }

                for( String url : relate.getValue() ) {
                    MeshObject neighbor = findOrCreate( url, mb );
                    if( neighbor.getIsDead() ) { // not sure this can happen here?
                        continue;
                    }
                    obj.blessRole( WebSubjectArea.ANY_MENTIONS_WEBRESOURCE_S, neighbor );
                    obj.setRoleAttributeValue( neighbor, CREATOR, CREATOR_NAME );
                }
            }

            for( Map.Entry<MeshObject,Set<String>> unrelate : unrelateThose.entrySet() ) {
                MeshObject obj = unrelate.getKey();
                if( obj.getIsDead() ) {
                    continue;
                }

                for( String url : unrelate.getValue() ) {
                    MeshObject neighbor = findOrCreate( url, mb );
                    if( neighbor.getIsDead() ) {
                        continue;
                    }
                    obj.unblessRole( WebSubjectArea.ANY_MENTIONS_WEBRESOURCE_S, neighbor );
                    obj.deleteRoleAttribute( neighbor, CREATOR );
                }
            }
        });
    }

    /**
     * Find or create the MeshObject that represents this URL.
     */
    protected MeshObject findOrCreate(
            String   url,
            MeshBase mb )
        throws
            MalformedURLException
    {
        URL u = new URL( url );

        EnumeratedValue protocol;
        StringBuilder buf = new StringBuilder();
        buf.append( u.getHost() );
        if( "http".equals( u.getProtocol())) {
            protocol = WebSubjectArea.WEBRESOURCE_PROTOCOL_type_HTTP;
            if( u.getPort() != -1 && u.getPort() != 80 ) {
                buf.append( ':' ).append( u.getPort() );
            }
        } else {
            protocol = WebSubjectArea.WEBRESOURCE_PROTOCOL_type_HTTPS;
            if( u.getPort() != -1 && u.getPort() != 443 ) {
                buf.append( ':' ).append( u.getPort() );
            }
        }
        buf.append( u.getPath() );
        if( u.getQuery() != null ) {
            buf.append( '?' ).append( u.getQuery() );
        }

        MeshObjectIdentifier id  = mb.createMeshObjectIdentifier( buf.toString() );
        MeshObject           ret = mb.findMeshObjectByIdentifier( id );

        if( ret == null ) {
            ret = mb.createMeshObject( id, WebSubjectArea.WEBRESOURCE );

            ret.setPropertyValue( WebSubjectArea.WEBRESOURCE_PROTOCOL, protocol );
        }

        return ret;
    }

    /**
     * Returns the Set of URLs found in this String.
     */
    protected Set<String> extractUrlsFrom(
            String s )
    {
        Set<String> ret = null;

        Matcher m = theHttpUrlPattern.matcher( s );
        while( m.find() ) {
            String url = m.group();

            if( ret == null ) {
                ret = new HashSet<>();
            }
            ret.add( url );
        }
        return ret;
    }

    /**
     * The regex used for looking for URLs. From https://ihateregex.io/expr/url/
     * with minor changes.
     */
    protected static final Pattern theHttpUrlPattern = Pattern.compile(
            "https?://[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()!@:%_\\+.~#?&/=]*)" );

    /**
     * The Attribute name indicating who did this.
     */
    public static final String CREATOR = "bot-created";

    /**
     * The Attribute value for me as creator.
     */
    public static final String CREATOR_NAME = UrlExtractorBot.class.getName();
}
