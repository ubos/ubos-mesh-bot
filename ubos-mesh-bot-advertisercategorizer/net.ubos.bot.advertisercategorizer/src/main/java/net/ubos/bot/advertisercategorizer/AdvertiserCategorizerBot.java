//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot.advertisercategorizer;

import com.google.gson.stream.JsonReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import net.ubos.bot.Bot;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectBlessChange;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.model.Structure.StructureSubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;

/**
 *
 */
public class AdvertiserCategorizerBot
    implements
        Bot
{
    private static final Log log = Log.getLogInstance( AdvertiserCategorizerBot.class );

    /**
     * {@inheritDoc}
     */
    @Override
    public void notifyChanges(
            ChangeList changes )
    {
        List<MeshObject> newAdvertisers = new ArrayList<>();

        for( Change change : changes ) {
            if( change instanceof MeshObjectBlessChange ) {
                MeshObjectBlessChange realChange = (MeshObjectBlessChange) change;

                for( EntityType addedType : realChange.getEntityTypes() ) {
                    if( addedType.isSubtypeOfOrEquals( MarketingSubjectArea.ADVERTISER )) {
                        newAdvertisers.add( realChange.getSource() );
                    }
                }
            }
        }

        if( newAdvertisers.isEmpty()) {
            return;
        }

        readCategoriesIfNeeded();

        MeshBase mb = changes.getMeshBase();

        mb.sudoExecute( () -> {
             for( MeshObject advertiser : newAdvertisers ) {
                 placeInCategories( advertiser, theIndustryCategoriesTable, AdvertiserCategorizerBot::findOrCreateCategory );
                 placeInCategories( advertiser, theLocationsTable,          AdvertiserCategorizerBot::findOrCreateLocation );
             }
        });
    }

    /**
     * Place this advertiser into the right categories.
     *
     * @param advertiser the advertiser
     * @param categoriesTable defines the categories
     * @param roleType the relationship to instantiate
     */
    public static void placeInCategories(
            MeshObject                             advertiser,
            Map<String,String>                     categoriesTable,
            BiFunction<String,MeshBase,MeshObject> findOrCreate )
    {
        StringValue nameValue = (StringValue) advertiser.getPropertyValue( MarketingSubjectArea.ADVERTISER_NAME );
        if( nameValue == null || nameValue.value().isBlank() ) {
            return;
        }

        // This may be a multi-word string, like "Audi Tysons Corner", and we want to match
        // "Audi" and "Tysons Corner". So splitting by word is not a good plan.
        String [] nameComponents = nameValue.value().trim().toLowerCase().replaceAll( "[-;:'\"\\.!,\\+\\-\\(\\)/]", " " ).split( "\\s+" );
        LinkedList<String []> remainingSearches = new LinkedList<>();
        remainingSearches.add( nameComponents );

        outer:
        while( !remainingSearches.isEmpty()) {
            String [] searchComponents = remainingSearches.removeFirst();

            // try the longest streak first
            for( int nComponents = searchComponents.length ; nComponents >= 1 ; --nComponents ) {

                for( int start = 0 ; start <= searchComponents.length - nComponents ; ++start ) {
                    StringBuilder attemptedKeyBuf = new StringBuilder();
                    String        sep             = "";
                    for( int i=0 ; i<nComponents ; ++i ) {
                        attemptedKeyBuf.append( sep );
                        attemptedKeyBuf.append( searchComponents[start+i] );
                        sep = " ";
                    }
                    String  attemptedKey = attemptedKeyBuf.toString();

                    String foundCategoryName = categoriesTable.get( attemptedKey );
                    if( foundCategoryName != null ) {
                        MeshObject category = findOrCreate.apply( foundCategoryName, advertiser.getMeshBase() );

                        if( category != null && !advertiser.isRelated( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S, category )) {
                            advertiser.blessRole( MarketingSubjectArea.ADVERTISER_ISIN_ADVERTISERCATEGORY_S, category );
                        }

                        // when this level is done, terminate this search;
                        // eliminate the found words from the remaining search;
                        // add a "before" and "after" search from the found words (We don't want to look for
                        // "a d" if the words had been "a b c d" and we just found "b c".

                        if( start > 0 ) {
                            remainingSearches.add( ArrayHelper.copyIntoNewArray( nameComponents, 0, start, String.class ));
                        }
                        if( start + nComponents < searchComponents.length ) {
                            remainingSearches.add( ArrayHelper.copyIntoNewArray( searchComponents, start + nComponents, searchComponents.length, String.class ));
                        }
                        continue outer;
                    }
                }
            }
        }
    }

    /**
     * Find or create a category MeshObject for his named category.
     */
    public static MeshObject findOrCreateCategory(
            String     name,
            MeshBase   mb )
    {
        try {
            MeshObjectIdentifier categoryId = mb.createMeshObjectIdentifierBelow( "industries", "category", name );
            MeshObject           ret        = mb.findMeshObjectByIdentifier( categoryId );

            if( ret == null ) {
                ret = mb.createMeshObject( categoryId, MarketingSubjectArea.INDUSTRYCATEGORY );
                ret.setPropertyValue( MarketingSubjectArea.ADVERTISERCATEGORY_NAME, StringValue.create( name ));
                ret.setAttributeValue( CREATOR, CREATOR_NAME );
            }
            return ret;

        } catch( ParseException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * Find or create a location MeshObject for this named location.
     */
    protected static MeshObject findOrCreateLocation(
            String   name,
            MeshBase mb )
    {
        try {
            MeshObjectIdentifier locationId = mb.createMeshObjectIdentifierBelow( "industries", "location", name );
            MeshObject           ret        = mb.findMeshObjectByIdentifier( locationId );

            if( ret == null ) {
                ret = mb.createMeshObject( locationId, MarketingSubjectArea.ADVERTISERLOCATION );
                ret.setPropertyValue( MarketingSubjectArea.ADVERTISERCATEGORY_NAME, StringValue.create( name ));
                ret.setAttributeValue( CREATOR, CREATOR_NAME );

                MeshObjectIdentifier locationsId = mb.createMeshObjectIdentifierBelow( "industries", "location" );
                MeshObject           locations   = mb.findMeshObjectByIdentifier( locationsId );

                if( locations == null ) {
                    locations = mb.createMeshObject( locationsId, MarketingSubjectArea.ADVERTISERLOCATIONCOLLECTION );
                }
                locations.blessRole( StructureSubjectArea.COLLECTION_COLLECTS_ANY_S , ret);
            }
            return ret;

        } catch( ParseException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * Helper to read the categories definitions the first time it is needed.
     */
    protected static void readCategoriesIfNeeded()
    {
        if( !theIndustryCategoriesTable.isEmpty() ) {
            return;
        }

        readTable( theIndustryCategoriesTable, "industries.json" );
        readTable( theLocationsTable,          "locations.json" );
    }

    /**
     * Helper to read a categorization table.
     *
     * @param table the table to read into
     * @param fileName the file to read
     */
    protected static void readTable(
            Map<String,String> table,
            String             fileName )
    {
        // invert the structure for faster lookup
        try {
            Reader     r  = new FileReader( "/ubos/share/ubos-mesh-bot-advertisercategorizer/" + fileName );
            JsonReader jr = new JsonReader( r );

            jr.beginObject();

            while( jr.hasNext() ) {
                String category = jr.nextName();
                jr.beginArray();

                while( jr.hasNext() ) {
                    String word = jr.nextString();

                    table.put( word.toLowerCase(), category );
                }

                jr.endArray();
            }
            jr.endObject();

        } catch( IOException ex ) {
            log.error( ex );
        }
    }

    /**
     * The known industry categories.
     */
    protected static final HashMap<String,String> theIndustryCategoriesTable = new HashMap<>();

    /**
     * The known locations.
     */
    protected static final HashMap<String,String> theLocationsTable = new HashMap<>();

    /**
     * The Attribute name indicating who did this.
     */
    public static final String CREATOR = "bot-created";

    /**
     * The Attribute value for me as creator.
     */
    public static final String CREATOR_NAME = AdvertiserCategorizerBot.class.getName();
}
